﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }

        public static ComplexNum operator + (ComplexNum num1, ComplexNum num2)
        { 
            return new ComplexNum(num1.Re + num2.Re, num1.Im + num2.Im);
        }

        public static ComplexNum operator - (ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum(num1.Re - num2.Re, num1.Im - num2.Im);
        }

        public static ComplexNum operator * (ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum(num1.Re * num2.Re - num1.Im * num2.Im, 
                                  num1.Re * num2.Im + num1.Im * num2.Re);
        }

        public static ComplexNum operator / (ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum((num1.Re * num2.Re + num1.Im * num2.Im)/
                                  (num2.Re * num2.Re + num2.Im * num2.Im),
                                  (num1.Im * num2.Re - num1.Re * num2.Im)/
                                  (num2.Re * num2.Re + num2.Im * num2.Im));
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate()
        {
            return new ComplexNum(this.Re, -this.Im);
        }
        
        public ComplexNum Invert()
        {
            return new ComplexNum(this.Re / (this.Re * this.Re + this.Im * this.Im),
                                 -this.Re / (this.Re * this.Re + this.Im * this.Im));
        }
        // Restituisce il modulo del numero complesso
        public double Module ()
        {
            return Math.Sqrt(this.Re * this.Re + this.Im * this.Im);
        }

        

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            if (this.Im == 0)
            {
                return "" + this.Re;
            }
            else
            {
                return this.Re > 0 ? this.Im + "i +" + this.Re :
                                     this.Im + "i -" + this.Im;
            }
        }
    }
}
