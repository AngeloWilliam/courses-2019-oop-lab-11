﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;
        private static int TOTEN = 10;

        public Deck()
        {
            this.cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length];
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano 
             * rispettivamente semi e valori di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni 
             * possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            int i = 0;
            foreach(ItalianSeed seed in (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed)))
            {
                foreach(ItalianValue value in (ItalianValue[])Enum.GetValues(typeof(ItalianValue)))
                {
                    this.cards[i] = new Card(seed.ToString(), value.ToString());
                    i++;
                }
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach (Card card in this.cards)
            {
                Console.WriteLine(card.ToString());
            }
        }

        public Card Indexer(ItalianSeed seed, ItalianValue value)
        {
            return cards[(int)seed*Deck.TOTEN + (int)value];
        }
    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
