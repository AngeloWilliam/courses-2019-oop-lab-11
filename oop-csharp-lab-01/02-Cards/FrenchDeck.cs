﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private static readonly int TOTEN = 13;
        private static readonly int BLACK_JOKER = 52;
        private static readonly int RED_JOKER = 53;
        private Card[] cards;
        
        public FrenchDeck()
        {
            this.cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length
                + Enum.GetNames(typeof(Joker)).Length];
        }

        public void Init()
        {
            int i = 0;

            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach(FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    this.cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }

            foreach(Joker joker in (Joker[])Enum.GetValues(typeof(Joker)))
            {
                this.cards[i] = new Card(joker.ToString(), joker.ToString());
                i++;
            }
        }

        public void Print()
        {
            foreach (Card card in this.cards)
            {
                Console.WriteLine(card.ToString());
            }
        }

        public Card Index(FrenchSeed seed, FrenchValue value)
        {
            return this.cards[(int)seed * FrenchDeck.TOTEN + (int)value];
        }

        public Card Index(Joker joker)
        {
            if (joker == Joker.BLACK_JOKER)
            {
                return this.cards[FrenchDeck.BLACK_JOKER];
            }
            else
            {
                return this.cards[FrenchDeck.RED_JOKER];
            }
        }
    }
    
    enum FrenchSeed
    {
        HEARTS,
        DIAMONDS,
        CLUBS,
        SPADES
    }

    enum FrenchValue
    {
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING
    }

    enum Joker
    {
        BLACK_JOKER,
        RED_JOKER
    }
}
